import { type WeatherAPIResponse } from '../src/getWeatherData';
import { computePrecipitationScore } from '../src/computePrecipitationScore';
import { test, expect,  } from '@jest/globals'

test('Sunny', () => {
    const weatherData = {
        current: {
            condition: {
                code: 1000
            }
        }
    } as WeatherAPIResponse

    const condition = computePrecipitationScore(weatherData)
    expect(condition.score).toBe(3)
})

test('Partly Cloudy', () => {
    const weatherData = {
        current: {
            condition: {
                code: 1003
            }
        }
    } as WeatherAPIResponse

    const condition = computePrecipitationScore(weatherData)
    expect(condition.score).toBe(2)
})

test('Cloudy', () => {
    const weatherData = {
        current: {
            condition: {
                code: 1006
            }
        }
    } as WeatherAPIResponse

    const condition = computePrecipitationScore(weatherData)
    expect(condition.score).toBe(1)
})

test('Overcast', () => {
    const weatherData = {
        current: {
            condition: {
                code: 1009
            }
        }
    } as WeatherAPIResponse

    const condition = computePrecipitationScore(weatherData)
    expect(condition.score).toBe(0)
})

test('Chance of rain', () => {
    const weatherData = {
        current: {
            condition: {
                code: 1069
            }
        }
    } as WeatherAPIResponse

    const condition = computePrecipitationScore(weatherData)
    expect(condition.score).toBe(-1)
})

test('Light Rain', () => {
    const weatherData = {
        current: {
            condition: {
                code: 1153
            }
        }
    } as WeatherAPIResponse

    const condition = computePrecipitationScore(weatherData)
    expect(condition.score).toBe(-1)
    expect(condition.description).toBe('It is raining (drizzle)')
})

test('Snow', () => {
    const weatherData = {
        current: {
            condition: {
                code: 1114
            }
        }
    } as WeatherAPIResponse

    const condition = computePrecipitationScore(weatherData)
    expect(condition.score).toBe(-3)
})
