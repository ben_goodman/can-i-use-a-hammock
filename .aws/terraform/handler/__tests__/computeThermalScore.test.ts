import { computeThermalScore } from '../src/computeThermalScore';
import { test, expect,  } from '@jest/globals'
import { WeatherAPIResponse } from '../src/getWeatherData';

test('Comfortable', () => {
  const mockWeatherData = {
    current: {
      temp_c: 26.0,
      feelslike_c: 26.0,
      wind_mph: 0.1,
      humidity: 55
    }
  } as WeatherAPIResponse

    const thermalScore = computeThermalScore(mockWeatherData, 0.5)
    expect(thermalScore).toStrictEqual(
      {
        score: 0,
        description: 'Comfortable',
        props: {
          clothingFactor: 0.5,
          metabolicRate: 1,
          externalWork: 0,
          airTemperature: 26,
          meanRadiantTemperature: 26,
          airVelocity: 0.044704,
          relativeHumidity: 55
        },
        PMV: 0.07
      }
    )
})

test('Warm', () => {
  const mockWeatherData = {
    current: {
      temp_c: 30.0,
      feelslike_c: 30.0,
      wind_mph: 0.1,
      humidity: 55
    }
  } as WeatherAPIResponse

    const thermalScore = computeThermalScore(mockWeatherData, 0.3)
    expect(thermalScore).toStrictEqual(
      {
        score: 1,
        description: 'Somewhat warm',
        props: {
          clothingFactor: 0.3,
          metabolicRate: 1,
          externalWork: 0,
          airTemperature: 30,
          meanRadiantTemperature: 30,
          airVelocity: 0.044704,
          relativeHumidity: 55
        },
        PMV: 1.4
      }
    )
})

test('Somewhat Cold', () => {
  const mockWeatherData = {
    current: {
      temp_c: 19.0,
      feelslike_c: 18.0,
      wind_mph: 0.1,
      humidity: 40
    }
  } as WeatherAPIResponse

    const thermalScore = computeThermalScore(mockWeatherData, 0.5)
    expect(thermalScore).toStrictEqual(
      {
        score: -1,
        description: 'Somewhat cold',
        props: {
          clothingFactor: 0.5,
          metabolicRate: 1,
          externalWork: 0,
          airTemperature: 19,
          meanRadiantTemperature: 18,
          airVelocity: 0.044704,
          relativeHumidity: 40
        },
        PMV: -2.89
      }
    )
})


