import { WeatherAPIResponse } from "./getWeatherData"

export interface PredictedMeanVoteProps {
    clothingFactor: number,
    metabolicRate: number,
    externalWork: number,
    airTemperature: number,
    meanRadiantTemperature: number,
    airVelocity: number,
    relativeHumidity: number
}

const fnps = (t) => Math.exp(16.6536 - 4030.183 / (t + 235) )

const predictedMeanVote = (props: PredictedMeanVoteProps) => {
    const CLO = props.clothingFactor
    const MET = props.metabolicRate
    const WME = props.externalWork
    const TA = props.airTemperature
    const TR = props.meanRadiantTemperature
    const VEL = props.airVelocity
    const RH = props.relativeHumidity

    const PA = RH * 10 * fnps(TA)
    const ICL = 0.155 * CLO
    const M = MET * 58.15
    const W = WME * 58.15
    const MW = M - W
    const FCL = ICL < 0.078 ? 1 + 1.29 * ICL : 1.05 + 0.645 * ICL
    const HCF = 12.1 * Math.sqrt(VEL)
    const TAA = TA + 273
    const TRA = TR + 273

    const TCLA = TAA + (35.5 - TA) / (3.5 * (6.45 * ICL + 0.1))
    const P1 = ICL * FCL
    const P2 = P1 * 3.96
    const P3 = P1 * 100
    const P4 = P1 * TAA
    const P5 = 308.7 - 0.028 * MW + P2 * (TRA/100)**4

    let XN = TCLA / 100

    let HC
    let XF = XN
    let N = 0

    const EPS = 0.00015

    XF = (XF + XN) / 2

    const HCN = 2.38 * Math.abs(100 * XF - TAA)**0.25

    HCF > HCN ? HC = HCF : HC = HCN

    XN = (P5 + P4 * HC - P2 * XF**4) / (100 + P3 * HC)

    while ( Math.abs(XN - XF) > EPS) {

        XF = (XF + XN) / 2

        const HCN = 2.38 * Math.abs(100 * XF - TAA)**0.25


        HCF > HCN ? HC = HCF : HC = HCN

        XN = (P5 + P4 * HC - P2 * XF**4) / (100 + P3 * HC)

        N++

        if (N > 150) {
            throw new Error('failed to converge')
        }
    }

    const TCL = 100 * XN - 273

    const HL1 = 3.05 * 0.001 * (5733 - 6.99 * MW - PA)

    let HL2
    if (MW > 58.15) {
        HL2 = 0.42 * (MW - 58.15)
    } else {
        HL2 = 0
    }

    const HL3 = 1.7 * 0.00001 * M * (5867 - PA)
    const HL4 = 0.0014 * M * (34 - TA)
    const HL5 = 3.96 * FCL * (XN ** 4 - (TRA / 100)**4 )
    const HL6 = FCL * HC * (TCL - TA)

    const TS = 0.303 * Math.exp((-0.036)*M) + 0.028
    const PMV = TS * (MW - HL1 - HL2 - HL3 - HL4 - HL5 - HL6)
    const PPD = 100 - 95 * Math.exp(-0.03353 * PMV**4 - 0.2179 * PMV**2)

    return {
        PMV: Math.round(PMV * 100) / 100,
        PPD: Math.round(PPD * 100) / 100,
    }
}

export interface ThermalScore {
    score: number,
    description: string,
    props: PredictedMeanVoteProps,
    PMV: number,
    PPD: number,
}

export const computeThermalScore = (
    weatherData: WeatherAPIResponse,
    clothingFactor: number,
    metabolicRate: number,
    externalWork: number
): ThermalScore => {

    const props = {
        clothingFactor,
        metabolicRate,
        externalWork,
        airTemperature: weatherData.current.temp_c,
        meanRadiantTemperature: weatherData.current.feelslike_c,
        airVelocity: weatherData.current.wind_mph  * 0.44704, // converts to m/s
        relativeHumidity: weatherData.current.humidity
    }

    const {PMV, PPD} =  predictedMeanVote(props)

    if (PMV > 2.5) {
        return {score: -1, description: 'Hot', props, PMV, PPD}
    }

    if (PMV < 2.5 && PMV >= 1.5) {
        return {score: 2, description: 'Warm', props, PMV, PPD}
    }

    if (PMV < 1.5 && PMV >= 0.5) {
        return {score: 1, description: 'Slightly warm', props, PMV, PPD}
    }

    if (PMV < 0.5 && PMV >= -0.5) {
        return {score: 0, description: 'Comfortable', props, PMV, PPD}
    }

    if (PMV < -0.5 && PMV >= -1.5) {
        return {score: -1, description: 'Slightly Cool', props, PMV, PPD}
    }

    if (PMV < -1.5 && PMV >= -2.5) {
        return {score: -2, description: 'Cool', props, PMV, PPD}
    }

    if (PMV < -2.5 && PMV >= -3.5) {
        return {score: -3, description: 'Cold', props, PMV, PPD}
    }

    return {score: -4, description: 'Very Cold', props, PMV, PPD}
}
