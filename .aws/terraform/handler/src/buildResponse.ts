import { computePrecipitationScore, type PrecipitationScore} from './computePrecipitationScore';
import { computeThermalScore, type ThermalScore } from "./computeThermalScore"
import { type WeatherAPIError, type WeatherAPIResponse } from "./getWeatherData"

export interface HammockFunctionResponse {
    isHammockViable: -1|0|1,
    thermalScore: ThermalScore,
    precipitationScore: PrecipitationScore,
    weatherData: WeatherAPIResponse['current'],
    location: WeatherAPIResponse['location'],
    error?: WeatherAPIError,
}

export const computeHammockViability = (
    thermalScore: number,
    precipitationScore: number
): HammockFunctionResponse['isHammockViable'] => {
    // a thermal score of >0 is considered comfortable
    // a precipitation score of >=0 is considered dry
    // both scores must be >=0 to be considered a good day for hammocking
    // a score of t=-1 is a maybe - either too hot or slightly cool
    // likewise, if p=0 then its also a maybe (overcast or rain is possible)

    // in all cases, it must be dry
    const isHammockViable = precipitationScore >= 0
        // 0 <= t-score < 2.5 is comfortable
        ? thermalScore >= 0 && thermalScore <= 2
            ? 1
            : thermalScore === -1
                ? 0
                : -1
        : -1

    return isHammockViable
}

export const buildResponse = (
    weatherData: WeatherAPIResponse,
    clothingFactor: number,
    metabolicRate: number,
    externalWork: number
): HammockFunctionResponse => {

    const thermalScore = computeThermalScore(weatherData, clothingFactor, metabolicRate, externalWork)
    const precipitationScore = computePrecipitationScore(weatherData)
    const isHammockViable = computeHammockViability(thermalScore.score, precipitationScore.score)

    return {
        isHammockViable,
        thermalScore,
        precipitationScore,
        weatherData: weatherData.current,
        location: weatherData.location,
    }
}