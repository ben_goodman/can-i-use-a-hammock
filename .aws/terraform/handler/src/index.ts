import { Context, APIGatewayProxyCallback, APIGatewayProxyEventV2 } from 'aws-lambda'
import queryString from 'query-string'
import { getWeatherData } from './getWeatherData'
import { buildResponse } from './buildResponse'

export const handler = (
    event: APIGatewayProxyEventV2,
    _context: Context,
    callback: APIGatewayProxyCallback
): void => {
    console.log('event', event)

    const location = event.rawPath.split('/')[2]
    const {
        clo = 0.36,
        met = 0.8,
        wme = 0
    } = queryString.parse(event.rawQueryString, {parseNumbers: true});

    console.log({location, clo, met, wme})

    getWeatherData(location)
        .then((weatherData) => {
            const response = buildResponse(weatherData, clo as number, met as number, wme as number)
            return callback(null, {
                statusCode: 200,
                body: JSON.stringify(response),
            })
        })
        .catch((error: Error) => {
            console.log('error: ', error)
            return callback(null, {
                statusCode: 500,
                body: error.message,
            })
        })
}
