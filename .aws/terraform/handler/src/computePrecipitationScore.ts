import { type WeatherAPIResponse } from "./getWeatherData"

export interface PrecipitationScore {
    score: number,
    description: string
}

export const computePrecipitationScore = (weatherData: WeatherAPIResponse): PrecipitationScore => {
    const {code} = weatherData.current.condition

    if (code === 1000) {
        return {score: 3, description: 'Clear skies.'}
    }

    if (code === 1003) {
        return {score: 2, description: 'Partly cloud skies.'}
    }

    if (code === 1006) {
        return {score: 1, description: 'Cloudy skies.'}
    }

    if (code === 1009 || code === 1030) {
        return {score: 0, description: 'It is Overcast'}
    }

    // rain is forecast
    if (code >= 1063 && code <= 1087) {
        return {score: 0, description: 'Rain is possible'}
    }

    // snowing
    if (code >= 1114 && code <= 1117) {
        return {score: -3, description: 'It is snowing'}
    }

    //fog
    if (code === 1135 || code === 1147) {
        return {score: -2, description: 'It is foggy'}
    }

    //minor rain
    if (code >= 1150 && code <= 1153) {
        return {score: -1, description: 'It is raining (drizzle).'}
    }

    //showers
    if (code === 1180 || code === 1183 || code === 1240) {
        return {score: -2, description: 'Light rain showers.'}
    }

    //freezing rain
    if (code === 1168 || code === 1171 || code >= 1198 && code <= 1207 || code === 1249 || code === 1252) {
        return {score: -3, description: 'It is raining (freezing).'}
    }

    //heavy rain
    if (code >= 1168 && code <= 1195 || code === 1243 || code === 1246) {
        return {score: -3, description: 'Moderate to heavy rain.'}
    }

    //snow
    if (code >= 1210 && code <= 1237 || code >= 1255 && code <= 1282) {
        return {score: -3, description: 'Snow is falling.'}
    }

    throw new Error(`Unknown weather code: ${code}`)
}