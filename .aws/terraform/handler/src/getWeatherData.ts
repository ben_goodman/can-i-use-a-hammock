export interface WeatherData {
    last_updated_epoch: number
    last_updated: string
    temp_c: number
    temp_f: number
    is_day: number
    condition: {
        text: string
        icon: string
        code: number
    }
    wind_mph: number
    wind_kph: number
    wind_degree: number
    wind_dir: string
    pressure_mb: number
    pressure_in: number
    precip_mm: number
    precip_in: number
    humidity: number
    cloud: number
    feelslike_c: number
    feelslike_f: number
    vis_km: number
    vis_miles: number
    uv: number
    gust_mph: number
    gust_kph: number
}

export interface WeatherAPIResponse {
    location: {
        name: string
        region: string
        country: string
        lat: number
        lon: number
        tz_id: string
        localtime_epoch: number
        localtime: string
    }
    current: WeatherData
}

export interface WeatherAPIError {
    error: {
        code: number
        message: string
    }
}

export const getWeatherData = async (location: string): Promise<WeatherAPIResponse> => {
    const url = `http://api.weatherapi.com/v1/current.json?key=${process.env.WEATHER_API_KEY}&q=${location}&aqi=no`
    console.log('url: ', url)
    const response = await fetch(url)
    const weatherData = (await response.json()) as WeatherAPIResponse & Partial<WeatherAPIError>

    console.log('weatherData: ', weatherData)

    if (weatherData.error) {
        throw new Error(JSON.stringify(weatherData))
    }

    return weatherData as WeatherAPIResponse
}
