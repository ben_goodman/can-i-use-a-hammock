data "archive_file" "lambda_payload" {
    type             = "zip"
    source_file      = "${path.module}/dist/index.js"
    output_file_mode = "0666"
    output_path      = "${path.module}/dist/index.js.zip"
}

module "hammock_function" {
    source = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "1.3.0"
    org             = "bgoodman"
    project_name    = "caniuseahammock"
    lambda_payload  = data.archive_file.lambda_payload
    publish         = true
    memory_size     = 512
}

resource "aws_lambda_function_url" "hammock_function_url" {
  function_name      = module.hammock_function.name
  authorization_type = "NONE"
}