resource "aws_cloudfront_cache_policy" "default" {
    name        = "default-cache-policy-${random_id.cd_function_suffix.hex}-${var.project_name}-${var.resource_namespace}"
    comment     = "${var.project_name}-${var.resource_namespace}"
    default_ttl = 3600
    max_ttl     = 86400
    min_ttl     = 0
    parameters_in_cache_key_and_forwarded_to_origin {
        cookies_config {
            cookie_behavior = "all"
        }
        headers_config {
            header_behavior = "none"
        }
        query_strings_config {
            query_string_behavior = "all"
        }
        enable_accept_encoding_brotli = true
        enable_accept_encoding_gzip = true
    }
}

resource "aws_cloudfront_cache_policy" "hammock_function" {
    name        = "hammock-function-cache-policy-${random_id.cd_function_suffix.hex}-${var.project_name}-${var.resource_namespace}"
    comment     = "${var.project_name}-${var.resource_namespace}"
    default_ttl = 5
    max_ttl     = 5
    min_ttl     = 0
    parameters_in_cache_key_and_forwarded_to_origin {
        cookies_config {
            cookie_behavior = "all"
        }
        headers_config {
            header_behavior = "none"
        }
        query_strings_config {
            query_string_behavior = "all"
        }
        enable_accept_encoding_brotli = true
        enable_accept_encoding_gzip = true
    }
}

module "website" {
    source  = "gitlab.com/ben_goodman/s3-website/aws"
    version = "2.0.0"

    org          = var.resource_namespace
    project_name = var.project_name
    aliases      = var.website_aliases
    use_cloudfront_default_certificate = false
    acm_certificate_arn = var.acm_certificate_arn
    default_cache_policy_id = aws_cloudfront_cache_policy.default.id
    default_response_headers_policy_id = aws_cloudfront_response_headers_policy.security_headers.id


    additional_custom_origins = {
        "hammock-report" = {
            domain_name = trimsuffix(trimprefix(aws_lambda_function_url.hammock_function_url.function_url, "https://"), "/")
        }
    }

    ordered_cache_behaviors = {
        "hammock-report" = {
            path_pattern = "/api/*"
            cache_policy_id = aws_cloudfront_cache_policy.hammock_function.id
            response_headers_policy_id = aws_cloudfront_response_headers_policy.security_headers.id
        }
    }

}

module "dns_alias" {
    source  = "gitlab.com/ben_goodman/s3-website/aws//modules/dns"
    version = "2.0.0"

    hosted_zone_name       = var.hosted_zone_name
    cloudfront_domain_name = module.website.cloudfront_default_domain
    domain_name            = var.website_aliases
}