project_name = "caniuseahammock"
resource_namespace = "bgoodman"
hosted_zone_name = "caniuseahammock.com"
website_aliases  = ["caniuseahammock.com", "www.caniuseahammock.com"]
acm_certificate_arn = "arn:aws:acm:us-east-1:757687723154:certificate/2b456d00-38d9-4749-acf7-77be988f7630"
