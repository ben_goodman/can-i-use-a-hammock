import React, { useState } from 'react'
import styled from 'styled-components'

const Row = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-top: 1em;
`

const StyledSelector = styled.select`
    margin: 0.5em;
    padding: 0.5em;
    width: 400px;
`

const StyledInput = styled.input`
    margin: 0.5em;
    padding: 0.5em;
    width: 400px;
`

export interface ClothingInputProps {
    onChange?: (clo: number) => void
}

const clothingOptions = [
    {
        value: 0.36,
        label: 'Walking shorts, short-sleeved shirt'
    },
    {
        value: 0.57,
        label: 'Trousers, short-sleeved shirt'
    },
    {
        value: 0.61,
        label: 'Trousers, long-sleeved shirt'
    },
    {
        value: 0.87,
        label: 'Same as above, plus vest.'
    }

]

export const ClothingInput = ({
    onChange = () => {}
}: ClothingInputProps) => {
    const [clo, setClo] = useState<number>(0.36)

    const handleChange = (e: React.ChangeEvent<HTMLSelectElement|HTMLInputElement>) => {
        const clo = parseFloat(e.target.value)
        setClo(clo)
        onChange(clo)
    }

    return (
        <>
            <p><a href='https://en.wikipedia.org/wiki/Clothing_insulation' target="_blank" rel="noopener noreferrer">Clothing Insulation</a></p>
            <Row>
                <StyledInput type="number" value={clo} onChange={handleChange} />
                <StyledSelector onChange={handleChange}>
                    {
                        clothingOptions.map((option, key) => (
                            <option key={`clo-${key}`} value={option.value.toString()}>{option.label}</option>
                        ))
                    }
                </StyledSelector>
            </Row>
        </>
    )
}