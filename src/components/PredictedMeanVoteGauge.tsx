import { HammockFunctionResponse } from '.aws/terraform/handler/src/buildResponse'
import React from 'react'
import GaugeComponent from 'react-gauge-component'

export interface ThermalScoreGaugeProps {
    thermalScore: HammockFunctionResponse['thermalScore']
}

export const PredictedMeanVoteGauge = ({
    thermalScore
}: ThermalScoreGaugeProps) => {
    // clamp PMV between -3.5 and 3.5
    const PMV = thermalScore.PMV
    const pmvClamped = Math.min(3.5, Math.max(-3.5, PMV))

    return <GaugeComponent
        id="gauge-component-pmv"
        type='radial'
        arc={{
            width: 0.1,
            padding: 0.005,
            cornerRadius: 1,
            subArcs: [
                {
                    limit: -3,
                    color: '#EA4228',
                    showTick: true
                },
                {
                    limit: -2.5,
                    color: '#EA4228',
                    showTick: true
                },
                {
                    limit: -1.5,
                    color: '#EA4228',
                    showTick: true
                },
                {
                    limit: -0.5,
                    color: '#F58B19',
                    showTick: true
                },
                {
                    limit: 0.5,
                    color: '#5BE12C',
                    showTick: true
                },
                {
                    limit: 1.5,
                    color: '#5BE12C',
                    showTick: true
                },
                {
                    limit: 2.5,
                    color: '#5BE12C',
                    showTick: true
                },
                {
                    limit: 3,
                    color: '#F58B19',
                    showTick: true
                },
            ]
        }}
        labels={{
            valueLabel: {
                formatTextValue: (value: number) => `${PMV < -3.5 ? PMV : value }`
            },
            tickLabels: {
                defaultTickLineConfig: {
                    color: '#000',
                },
                defaultTickValueConfig: {
                    style: {fill: '#000', fontSize: '18px'},
                }
            }
        }}
        minValue={-3.5}
        maxValue={3.5}
        value={pmvClamped}
        pointer={{
            type: 'arrow',
            color: '#000',
            elastic: false,
        }}
    />
}
