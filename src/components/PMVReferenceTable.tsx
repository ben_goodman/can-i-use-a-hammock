import React from 'react'
import { StyledTable } from './StyledTable'

export const PMVReferenceTable = () => {
    return <StyledTable>
        <thead>
            <tr>
                <th>PMV</th>
                <th>Thermal Sensation</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>-3</td>
                <td>Cold</td>
            </tr>
            <tr>
                <td>-2</td>
                <td>Cool</td>
            </tr>
            <tr>
                <td>-1</td>
                <td>Slightly Cool</td>
            </tr>
            <tr>
                <td>0</td>
                <td>Neutral</td>
            </tr>
            <tr>
                <td>1</td>
                <td>Slightly Warm</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Warm</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Hot</td>
            </tr>
        </tbody>
    </StyledTable>
}