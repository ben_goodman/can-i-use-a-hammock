import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { useGeolocation } from 'src/services/useGeolocation'


const Column = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 1em;

    input, button {
        margin: 0.5em;
        padding: 0.5em;
        width: 400px;
    }
`

export interface LocationInputProps {
  onSubmit?: (location: string) => void
}

export const LocationInput = ({
    onSubmit = () => {}
}: LocationInputProps) => {

    const [locationField, setLocationField] = useState<string>('')
    const [latLon, setLatLon] = useState<string|undefined>(undefined)

    const {loading, geolocation, permission} = useGeolocation()

    useEffect(() => {
        if (!loading && geolocation && geolocation.coords) {
            const {latitude, longitude} = geolocation.coords
            setLatLon(`${latitude},${longitude}`)
        }
    }, [loading, geolocation])

    const handleInput = (e: React.ChangeEvent<HTMLInputElement> ) => {
        const location = e.target.value
        setLocationField(location)
    }

    const handleManualSubmit = () => {
        if (locationField !== '') {
            onSubmit(locationField)
        }
    }

    const handleAutomaticSubmit = () => {
        onSubmit(latLon!)
    }

    return (
        <Column>
            <input type="text" placeholder='Enter a zip/postal code, city name, etc.' value={locationField} onChange={handleInput} />
            <button onClick={handleManualSubmit}>Submit</button>

            <button onClick={handleAutomaticSubmit} disabled={!latLon}>
                {permission ? loading ? 'Finding Your Location...' : 'Use My Current Location' : 'Use My Current Location (Disabled)'}
            </button>
        </Column>
    )
}
