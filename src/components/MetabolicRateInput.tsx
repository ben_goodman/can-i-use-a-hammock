import React, { useState } from 'react'
import styled from 'styled-components'

const Row = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-top: 1em;
`

const StyledSelector = styled.select`
    margin: 0.5em;
    padding: 0.5em;
    width: 400px;
`

const StyledInput = styled.input`
    margin: 0.5em;
    padding: 0.5em;
    width: 400px;
`

export interface MetabolicRateInputProps {
    onChange?: (clo: number) => void
}

const metabolicRateOption = [
    {
        value: 0.8,
        label: 'Reclining'
    },
    {
        value: 1.0,
        label: 'Seated, relaxed'
    },
    {
        value: 1.2,
        label: 'Sedentary activity, (office, school, etc.)'
    },
    {
        value: 1.6,
        label: 'Standing, light activity (office, lab, etc.).'
    },
    {
        value: 1.6,
        label: 'Standing, medium activity (shop assistant, domestic work).'
    },
    {
        value: 1.9,
        label: 'Walking 2 km/h'
    },
    {
        value: 2.4,
        label: 'Walking 3 km/h'
    },
    {
        value: 2.8,
        label: 'Walking 4 km/h'
    },
    {
        value: 3.4,
        label: 'Walking 5 km/h'
    },
]

export const MetabolicRateInput = ({
    onChange = () => {}
}: MetabolicRateInputProps) => {
    const [met, setMet] = useState<number>(0.8)

    const handleChange = (e: React.ChangeEvent<HTMLSelectElement|HTMLInputElement>) => {
        const clo = parseFloat(e.target.value)
        setMet(clo)
        onChange(clo)
    }

    return (
        <>
            <p><a href='https://en.wikipedia.org/wiki/Thermal_comfort#Metabolic_rate' target="_blank" rel="noopener noreferrer">Metabolic Rate</a></p>
            <Row>
                <StyledInput type="number" value={met} onChange={handleChange} />
                <StyledSelector onChange={handleChange}>
                    {
                        metabolicRateOption.map((option, key) => (
                            <option key={`clo-${key}`} value={option.value.toString()}>{option.label}</option>
                        ))
                    }
                </StyledSelector>
            </Row>
        </>
    )
}