import React from 'react'
import styled from 'styled-components'
import { HammockFunctionResponse } from '.aws/terraform/handler/src/buildResponse'
import { PredictedMeanVoteGauge } from './PredictedMeanVoteGauge'
import { PMVReferenceTable } from './PMVReferenceTable'
import { ThermalScoreProperties } from './ThermalScoreProperties'

const SummaryParagraph = styled.p`
    font-size: 1.5em;
`

export interface DisplayResultsProps {
    data: HammockFunctionResponse
}

export const DisplayResults = ({
    data
}: DisplayResultsProps) => {
    return (
        <>
            <SummaryParagraph>{
                data.isHammockViable === 1
                    ? 'Yes, you can use a hammock.'
                    : data.isHammockViable === 0
                        ? 'Maybe you can use a hammock, but consider all the factors first.'
                        : 'No, you should not use a hammock.'
            }</SummaryParagraph>

            <ul>
                <li>{data.thermalScore.description}</li>
                {data.precipitationScore.score <= 0 && <li>{data.precipitationScore.description}</li>}
            </ul>

            <hr />

            <h3>
                Thermal Sensation (<a href='https://en.wikipedia.org/wiki/Thermal_comfort' target="_blank" rel="noopener noreferrer">Predicted Mean Vote)</a>
            </h3>

            <p>PMV represents the &quot;Predicted mean vote&quot; (on the thermal sensation scale) of a large population of people exposed to a certain environment. PMV is derived from the physics of heat transfer combined with an empirical fit to sensation. PMV establishes a thermal strain based on steady-state heat transfer between the body and the environment.</p>

            <p>Using the latest weather data for {data.location.name}, the outdoor thermal sensation for your area is: <b>{data.thermalScore.description}</b></p>

            <PredictedMeanVoteGauge thermalScore={data.thermalScore} />

            <PMVReferenceTable />

            <h3>Model Properties</h3>

            The PMV index can be determined when the physical activity (metabolic rate) and clothing (thermal resistance) are estimated. The following properties were used to calculate the PMV index:

            <ThermalScoreProperties thermalScore={data.thermalScore} />

            <h3>
                Precipitation
            </h3>
            <p>{data.precipitationScore.description}</p>

        </>
    )
}