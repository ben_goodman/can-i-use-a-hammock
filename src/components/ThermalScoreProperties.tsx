import React from 'react'
import { HammockFunctionResponse } from '.aws/terraform/handler/src/buildResponse'
import { StyledTable } from './StyledTable'

interface ThermalScorePropertiesProps {
    thermalScore: HammockFunctionResponse['thermalScore']
}

export const ThermalScoreProperties = ({
    thermalScore
}: ThermalScorePropertiesProps) => {
    return (
        <StyledTable>
            <thead>
                <tr>
                    <th>Thermal Property</th>
                    <th>Value</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Clothing Factor</td>
                    <td>{thermalScore.props.clothingFactor} clo</td>
                </tr>
                <tr>
                    <td>Metabolic Rate</td>
                    <td>{thermalScore.props.metabolicRate} met</td>
                </tr>
                <tr>
                    <td>External Work</td>
                    <td>{thermalScore.props.externalWork} met</td>
                </tr>
                <tr>
                    <td>Air Temperature</td>
                    <td>{thermalScore.props.airTemperature}°C</td>
                </tr>
                <tr>
                    <td>Mean Radiant Temperature</td>
                    <td>{thermalScore.props.meanRadiantTemperature}°C</td>
                </tr>
                <tr>
                    <td>Air Velocity</td>
                    <td>{thermalScore.props.airVelocity.toFixed(2)} m/s</td>
                </tr>
                <tr>
                    <td>Relative Humidity</td>
                    <td>{thermalScore.props.relativeHumidity}%</td>
                </tr>
            </tbody>
        </StyledTable>
    )
}
