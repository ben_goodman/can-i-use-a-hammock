import React from 'react'
import { useParams, useLocation } from 'react-router-dom'
import { DisplayResults } from 'src/components/DisplayResults'
import { useHammockSuitability } from 'src/services/useHammockSuitability'

export const Component = () => {
    const { location } = useParams()
    const params = useLocation().search

    const opts = new URLSearchParams(params)
    const clo = opts.get('clo') ? parseFloat(opts.get('clo')!) : undefined
    const met = opts.get('met') ? parseFloat(opts.get('met')!) : undefined

    const {data, loading, error, errorMsg} = useHammockSuitability(location, {clo, met})

    if (loading) {
        return <p>loading</p>
    }

    if (error) {
        return <p>{errorMsg}</p>
    }


    return (
        <>
            <DisplayResults data={data!}/>
        </>
    )

}

Component.displayName = 'ViewConditions'