import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import queryString from 'query-string'
import { LocationInput } from 'src/components/LocationInput'
import { ClothingInput } from 'src/components/ClothingInput'
import { MetabolicRateInput } from 'src/components/MetabolicRateInput'

export const Component = () => {
    const navigate = useNavigate()

    const [clo, setClo] = useState<number | undefined>(undefined)
    const [met, setMet] = useState<number | undefined>(undefined)

    const onSubmit = (location: string) => {
        const params = queryString.stringify({clo, met})
        navigate(`/location/${location}?${params}`)
    }

    return (
        <>
            <div>
                <ClothingInput onChange={setClo} />
                <MetabolicRateInput onChange={setMet} />
            </div>

            <div>
                <p>Location:</p>
                <LocationInput onSubmit={onSubmit} />
            </div>
        </>
    )
}

Component.displayName = 'Home'