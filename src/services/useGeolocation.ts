import { useEffect, useState } from 'react'

export interface GeoLocationResponse {
    loading: boolean,
    permission: boolean,
    geolocation?: GeolocationPosition
}

const DEFAULT_RESPONSE: GeoLocationResponse = {
    loading: true,
    geolocation: undefined,
    permission: false
}

export const useGeolocation = () => {

    const [locationResponse, setLocation] = useState<GeoLocationResponse>(DEFAULT_RESPONSE)

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(handleSuccess)
        navigator.permissions.query({name:'geolocation'}).then(function(result) {
            // Will return ['granted', 'prompt', 'denied']
            if (result.state === 'granted') {
                console.log('geolocation granted.')
                setLocation(state => ({loading: state.loading, geolocation: state.geolocation, permission: true}))
            }
        })
    }, [])

    const handleSuccess = (geolocation: GeolocationPosition) => setLocation(state => {
        const newState = {permission: state.permission, geolocation, loading: false}
        return newState
    })

    return locationResponse
}