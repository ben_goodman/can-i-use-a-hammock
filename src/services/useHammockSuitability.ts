import React, { useEffect } from 'react'
import queryString from 'query-string'
import { type HammockFunctionResponse } from '../../.aws/terraform/handler/src/buildResponse'

export interface ModelProps {
    clo?: number,
    met?: number,
    wme?: number,
}

export interface HammockSuitabilityResponse {
    loading: boolean
    error: boolean
    errorMsg: string | undefined,
    data: HammockFunctionResponse | undefined
}

const DEFAULT_REQUEST = {
    loading: true,
    error: false,
    errorMsg: undefined,
    data: undefined,
}

export const useHammockSuitability = (location?: string, opts: ModelProps = {}): HammockSuitabilityResponse => {
    if (!location) {
        return DEFAULT_REQUEST
    }

    const [
        environmentData,
        setEnvironmentData
    ] = React.useState<HammockSuitabilityResponse>(DEFAULT_REQUEST)

    useEffect(() => {
        const modelProps = queryString.stringify(opts)
        setEnvironmentData(DEFAULT_REQUEST)
        fetch(`https://caniuseahammock.com/api/${location}?${modelProps}`)
            .then((response) => response.json())
            .then((data) => {
                setEnvironmentData({
                    loading: false,
                    error: false,
                    errorMsg: undefined,
                    data: data,
                })
            })
            .catch((error) => {
                setEnvironmentData({
                    loading: false,
                    error: true,
                    errorMsg: error,
                    data: undefined,
                })
            })
    }, [])

    return environmentData
}